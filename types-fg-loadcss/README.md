# Installation
> `npm install --save @types/fg-loadcss`

# Summary
This package contains type definitions for fg-loadcss (https://github.com/filamentgroup/loadCSS).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/fg-loadcss.

### Additional Details
 * Last updated: Thu, 19 Nov 2020 01:04:25 GMT
 * Dependencies: none
 * Global values: none

# Credits
These definitions were written by [Noah Overcash](https://github.com/ncovercash).
